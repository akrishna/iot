package org.ow2.choreos.mapping.interfaces;

import java.util.ArrayList;

import org.ow2.choreos.query.impl.Query;





public interface MappingGenerator {

	public ArrayList<String> getDeviceTypes(ArrayList<Query> substituteQueries);
	
	public ArrayList<String> getConcepts(ArrayList<Query> substituteQueries);
	
	//TODO decompose the query to get concepts and locations from the query and look for those 
	// in the ontology, THE QUERY HAS CONCEPTS AND CONDITIONS. LOCATION IS A CONDITION
}
