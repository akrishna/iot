package org.ow2.choreos.optimization.interfaces;

import java.util.ArrayList;

import org.ow2.chereos.iots.common.Function;
import org.ow2.choreos.lookup.impl.Device;





public interface OptimizationGenerator {

	public ArrayList<Device> getBestSet(ArrayList<Device> accessibleDevices,
			double accuracyRequirment, double coverageRequirements,
			ArrayList<Function> SubstFunctions);
}
