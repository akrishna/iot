package org.ow2.choreos.expansion.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import org.ow2.choreos.execution.impl.ExecutionGeneratorImpl;
import org.ow2.choreos.expansion.interfaces.ExpansionGenerator;
import org.ow2.choreos.lookup.impl.LookupManagerImpl;
import org.ow2.choreos.lookup.interfaces.LookupManager;
import org.ow2.choreos.query.impl.Query;
import org.ow2.choreos.queryiotsmanager.impl.ThingsQueryManagerImpl;
import org.ow2.choreos.sparqlfunctions.constants.Concept;
import org.ow2.choreos.sparqlfunctions.constants.DataFusionGeneratorImpl;
import org.ow2.choreos.sparqlfunctions.constants.DecayFormula;
import org.ow2.choreos.sparqlfunctions.constants.FinalResult;
import org.ow2.choreos.sparqlfunctions.constants.Measurement;
import org.ow2.choreos.sparqlfunctions.constants.Parameter;
import org.ow2.choreos.sparqlfunctions.constants.Unit;

import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;

import org.ow2.chereos.iots.common.Constants;
import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.iots.datatypes.DefaultBooleanData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleNoiseLevelData;
import org.ow2.choreos.iots.datatypes.DefaultDoublePressurePaData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleSoundData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleTemperatureCelsiusData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleTemperatureFahrenheitData;
import org.ow2.choreos.iots.datatypes.SensorData;

public class ExpansionGeneratorImpl implements ExpansionGenerator {
	//public static Query mainQuery;

	// TODO stop if there are no devices, dont call the next method
	@Override
	/**
	 * The method is called by the query manager to expand the application's query and provide
	 * all possible substitute queries that can replace the initial query and provide the same result
	 * @param queryToExpand: the initial query sent by the application
	 * @param fusionFunctionType: the type of the fusion function that should be applied to data
	 * gathered from services.
	 * @return a final numerical value that is the result of data fusion
	 */
	public SensorData expandQuery(Query queryToExpand,
			String fusionFunctionType, String requestedDataType)
			throws UnknownFunctionException, UnparsableExpressionException,
			NumberFormatException {

		// main = queryToExpand;
		HashMap<String, HashMap<SensorData, Location>> DataWithLoc = new HashMap<String, HashMap<SensorData, Location>>();

		String unit = null;
		// HashMap<String, SensorData> dataFromServices = new HashMap<String,
		// SensorData>();

		LookupManager le = new LookupManagerImpl();
		ExecutionGeneratorImpl eg = new ExecutionGeneratorImpl();
		DataFusionGeneratorImpl dg = new DataFusionGeneratorImpl();
		// find expansion which we refer to as subqueries
		ArrayList<SensorData> locations = new ArrayList<SensorData>();
		// find all sub-concepts that result from the expansion process,
		// sub-concepts should include the main concept as well
		ArrayList<Concept> expandedConcepts = queryToExpand.expandConcept();
		for (int i = 0; i < expandedConcepts.size(); i++) {
			System.out.println("concept is "
					+ expandedConcepts.get(i).getConceptName());
		}
		double finalDataVal = 0;

		ArrayList<Service> servicesToAccess = new ArrayList<Service>();

		try {
			// before requesting lookup, we need to know if concepts have a
			// decay formula, because if
			// they do the devices to select will have a normal distribution
			// around the location of interest

			ArrayList<Double> decayConstants = new ArrayList<Double>();
			for (Concept c : expandedConcepts) {
				DecayFormula df = new DecayFormula();
				decayConstants.add(df.computeDecay(c));
			}

			 servicesToAccess = le.findDevices(expandedConcepts,
			 queryToExpand.getLocationOfInterest());
			// TODO for now assume no composition so in the restclient i send
			// the first item in the decays list;
//
//			if (queryToExpand.getLocationOfInterest().getLocationType() == "point"
//					&& decayConstants.get(0) != 0)
////				servicesToAccess = le.findSubSetOfDevices(expandedConcepts,
////						queryToExpand.getLocationOfInterest(), decayConstants,
////						queryToExpand.getLocationOfInterest().getXCoordinate(),
////						queryToExpand.getLocationOfInterest().getYCoordinate(),
////						"normal", 20);
//			servicesToAccess = le.findSubSetOfDevicesBasedOnCoverage(expandedConcepts,
//					queryToExpand.getLocationOfInterest(), decayConstants,
//					queryToExpand.getLocationOfInterest().getXCoordinate(),
//					queryToExpand.getLocationOfInterest().getYCoordinate(),
//					"normal", 80, new Date().getTime());
//			// we check if the location of interest is a point that does not
//			// have a decay to request a
//			// uniform distribution with 6 sensors to select around the point .
//			else if (queryToExpand.getLocationOfInterest().getLocationType() == "point"
//					&& decayConstants.get(0) == 0) {
////				servicesToAccess = le.findSubSetOfDevices(expandedConcepts,
////						queryToExpand.getLocationOfInterest(), decayConstants,
////						queryToExpand.getLocationOfInterest().getXCoordinate(),
////						queryToExpand.getLocationOfInterest().getYCoordinate(),
////						"uniform", 6);
//				servicesToAccess = le.findSubSetOfDevicesBasedOnCoverage(expandedConcepts,
//						queryToExpand.getLocationOfInterest(), decayConstants,
//						queryToExpand.getLocationOfInterest().getXCoordinate(),
//						queryToExpand.getLocationOfInterest().getYCoordinate(),
//						"uniform", 80, new Date().getTime());
//			}
//			// // we check if the location of interest is an area and request
//			// // uniform distribution all over the area
//			else if (queryToExpand.getLocationOfInterest().getLocationType() == "area") {
//				servicesToAccess = le.findSubSetOfDevicesBasedOnCoverage(expandedConcepts,
//						queryToExpand.getLocationOfInterest(), "uniform", 80, new Date().getTime());
//			}

		} catch (NullPointerException e) {
			System.out
					.println("ExpansionGeneratorImpl.findDevices: no services found");
			return null;
		}
		try {
			// access services to get measurement values, which return the
			// address of the service and the measured value
			DataWithLoc = eg.getValues(servicesToAccess);

		} catch (NullPointerException e) {
			System.out
					.println("ExpansionGeneratorImpl.expandQuery: No data returned");
			return null;
		}

		// TODO return result before fusion too.

		// this hashmap is used by the compute method which needs the parameters
		// not the concepts as parameters are more specific and have a specific
		// unit
		// and a measurement
		HashMap<Parameter, Measurement> paramsAndValues = new HashMap<Parameter, Measurement>();
		String myDatType = null;
		if (DataWithLoc != null)
			// for each entry we get the type and the unit and create the
			// parameter object to which the measurement value
			// will be linked

			for (Entry<String, HashMap<SensorData, Location>> entry : DataWithLoc
					.entrySet()) {
				HashMap<SensorData, Location> myDatLoc = entry.getValue();
				for (Entry<SensorData, Location> locEntry : myDatLoc.entrySet()) {
					SensorData myDat = locEntry.getKey();
					myDatType = myDat.getUid();
					if (Constants.DOUBLE2DLOCATION.equals(myDatType) == false
							&& Constants.BOOLEAN.equals(myDatType) == false) {

						if (Constants.DOUBLETEMPERATUREC.equals(myDatType))
							unit = myDat.getComponentNames()[1];
						if (Constants.DOUBLETEMPERATUREF.equals(myDatType))
							unit = myDat.getComponentNames()[1];
						else if (Constants.DOUBLENOISE.equals(myDatType)
								|| Constants.DOUBLESOUND.equals(myDatType)
								|| Constants.DOUBLEPRESSUREPA.equals(myDatType))
							unit = myDat.getComponentNames()[1];
						// else if (Constants.DOUBLE.equals(myDatType))
						// unit = "";
						String concept = myDatType.substring(myDatType
								.lastIndexOf(".") + 1);
						System.out.println("unit is " + unit + " concept is "
								+ concept);
						System.out.println("data is " + myDat);
						paramsAndValues.put(
								new Parameter(concept.concat("_" + unit),
										new Unit(unit), new Concept(concept),
										concept.toString().subSequence(0, 1)
												.toString()),

								new Measurement(Double.parseDouble(myDat
										.toString()), locEntry.getValue()));

						// TODO add other conditions and for instance if they
						// are
						// integers then do
						// integer.parseInteger etc.
					} else if (Constants.DOUBLE2DLOCATION.equals(myDatType)) {
						unit = myDat.getComponentNames()[0] + ","
								+ myDat.getComponentNames()[1];
						System.out.println(myDat);
						locations.add(myDat);
					} else if (Constants.BOOLEAN.equals(myDatType)) {
						unit = "";
						return myDat;
					}
				}

			}
		if (Constants.DOUBLE2DLOCATION.equals(myDatType)) {

			double result = dg.generateFinalDataValues(locations,
					fusionFunctionType);
			return new DefaultDoubleData(result, 0);
		}
		// TODO update code because in some of it only the unit matters so i
		// check the unit returned by compute concept method
		HashMap<Concept, FinalResult> finalResultMap = queryToExpand
				.computeQueryConcept(paramsAndValues, fusionFunctionType);
		// TODO update for several concepts, for now we consider the query has
		// one concept only
		FinalResult finalAnswer = finalResultMap.get(queryToExpand
				.getConceptsToMeasure().get(0));
		finalDataVal = finalResultMap.get(
				queryToExpand.getConceptsToMeasure().get(0)).getResultValue();
		SensorData dataValSensorData = null;
		if (ThingsQueryManagerImpl.DATATYPE.equals(Constants.DOUBLENOISE)) {

			dataValSensorData = new DefaultDoubleNoiseLevelData(finalDataVal, 0);
			// since the compute method might return a result in a unit that is
			// different from the requested unit (in case not found
			// so that we dont return an empty result) we also check for that
			// unit before creating the sensor object
		} else if (ThingsQueryManagerImpl.DATATYPE
				.equals(Constants.DOUBLESOUND)
				&& finalAnswer.getResultUnit().equals("db")) {

			dataValSensorData = new DefaultDoubleSoundData(finalDataVal, 0);
		}

		else if (ThingsQueryManagerImpl.DATATYPE
				.equals("org.ow2.choreos.sensordata.double.pascal.pressure")) {
			System.out.println(finalAnswer.getResultUnit().getUnitName());
			dataValSensorData = new DefaultDoublePressurePaData(finalDataVal, 0);

		} else if (ThingsQueryManagerImpl.DATATYPE
				.equals(Constants.DOUBLETEMPERATUREC)
				&& finalAnswer.getResultUnit().getUnitName().equals("degreeC")) {

			dataValSensorData = new DefaultDoubleTemperatureCelsiusData(
					finalDataVal, 0);

		} else if (ThingsQueryManagerImpl.DATATYPE
				.equals(Constants.DOUBLETEMPERATUREF)
				&& finalAnswer.getResultUnit().getUnitName().equals("degreeF")) {

			dataValSensorData = new DefaultDoubleTemperatureFahrenheitData(
					finalDataVal, 0);

		}
		return dataValSensorData;

	}

	public boolean sendQuery(Query myQuery, String message,
			String requestedDataType) {
		boolean response;

		String unit = null;
		// HashMap<String, SensorData> dataFromServices = new HashMap<String,
		// SensorData>();

		LookupManager le = new LookupManagerImpl();
		ExecutionGeneratorImpl eg = new ExecutionGeneratorImpl();
		DataFusionGeneratorImpl dg = new DataFusionGeneratorImpl();
		// find expansion which we refer to as subqueries

		// find all sub-concepts that result from the expansion process,
		// sub-concepts should include the main concept as well
		Concept myConcept = myQuery.getConceptsToMeasure().get(0);

		ArrayList<Service> servicesToAccess = new ArrayList<Service>();

		try {

			servicesToAccess = le.findDevices(myConcept,
					myQuery.getLocationOfInterest());

		} catch (NullPointerException e) {
			System.out
					.println("ExpansionGeneratorImpl.findDevices: no services found");
			return false;
		}
		try {
			// access services to get measurement values, which return the
			// address of the service and the measured value
			response = eg.setValues(servicesToAccess, message);

		} catch (NullPointerException e) {
			System.out
					.println("ExpansionGeneratorImpl.expandQuery: No data returned");
			return false;
		}

		// TODO return result before fusion too.

		// this hashmap is used by the compute method which needs the parameters
		// not the concepts as parameters are more specific and have a specific
		// unit
		// and a measurement
		return response;
	}

	public HashMap<String, SensorData> expandQuery(Query myQuery,
			String requestedDataType) throws UnknownFunctionException,
			UnparsableExpressionException {

		// main = queryToExpand;
		HashMap<String, HashMap<SensorData, Location>> DataWithLoc = new HashMap<String, HashMap<SensorData, Location>>();

		String unit = null;
		HashMap<String, SensorData> finalMap = new HashMap<String, SensorData>();

		LookupManager le = new LookupManagerImpl();
		ExecutionGeneratorImpl eg = new ExecutionGeneratorImpl();
		DataFusionGeneratorImpl dg = new DataFusionGeneratorImpl();
		// find expansion which we refer to as subqueries

		// find all sub-concepts that result from the expansion process,
		// sub-concepts should include the main concept as well
		ArrayList<Concept> expandedConcepts = myQuery.expandConcept();
		for (int i = 0; i < expandedConcepts.size(); i++) {
			System.out.println("concept is "
					+ expandedConcepts.get(i).getConceptName());
		}

		ArrayList<Service> servicesToAccess = new ArrayList<Service>();

		try {
			// before requesting lookup, we need to know if concepts have a
			// decay formula, because if
			// they do the devices to select will have a normal distribution
			// around the location of interest

			ArrayList<Double> decayConstants = new ArrayList<Double>();
			for (Concept c : expandedConcepts) {
				DecayFormula df = new DecayFormula();
				decayConstants.add(df.computeDecay(c));
			}

			servicesToAccess = le.findDevices(expandedConcepts,
					myQuery.getLocationOfInterest());
			// TODO for now assume no composition so in the restclient i send
			// the first item in the decays list;

		} catch (NullPointerException e) {
			System.out
					.println("ExpansionGeneratorImpl.findDevices: no services found");
			return null;
		}
		try {
			// access services to get measurement values, which return the
			// address of the service and the measured value
			DataWithLoc = eg.getValues(servicesToAccess);

		} catch (NullPointerException e) {
			System.out
					.println("ExpansionGeneratorImpl.expandQuery: No data returned");
			return null;
		}

		if (DataWithLoc != null)
			// for each entry we get the type and the unit and create the
			// parameter object to which the measurement value
			// will be linked
			for (Entry<String, HashMap<SensorData, Location>> entry : DataWithLoc
					.entrySet()) {
				HashMap<SensorData, Location> myDatLoc = entry.getValue();
				for (Entry<SensorData, Location> locEntry : myDatLoc.entrySet()) {
					SensorData myDat = locEntry.getKey();
					String myDatType = myDat.getUid();

					if (Constants.BOOLEAN.equals(myDatType)) {
						unit = myDat.getComponentNames()[1];
						DefaultBooleanData dataValSensorData = new DefaultBooleanData(
								Boolean.parseBoolean(myDat.toString()), 0);

						finalMap.put(entry.getKey(), dataValSensorData);
					} else if (Constants.DOUBLENOISE.equals(myDatType)) {
						unit = myDat.getComponentNames()[1];
						DefaultDoubleNoiseLevelData dataValSensorData = new DefaultDoubleNoiseLevelData(
								Double.parseDouble(myDat.toString()), 0);

						finalMap.put(entry.getKey(), dataValSensorData);

					} else if (Constants.DOUBLEPRESSUREPA.equals(myDatType)) {
						unit = myDat.getComponentNames()[1];
						DefaultDoublePressurePaData dataValSensorData = new DefaultDoublePressurePaData(
								Double.parseDouble(myDat.toString()), 0);

						finalMap.put(entry.getKey(), dataValSensorData);

					}

				}
			}
		// TODO update code because in some of it only the unit matters so i
		// check the unit returned by compute concept method

		// TODO update for several concepts, for now we consider the query has
		// one concept only

		return finalMap;
	}
}
