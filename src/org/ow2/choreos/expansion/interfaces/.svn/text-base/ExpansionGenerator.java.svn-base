package org.ow2.choreos.expansion.interfaces;


import org.ow2.choreos.iots.datatypes.SensorData;
import org.ow2.choreos.query.impl.Query;

import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;



public interface ExpansionGenerator {
	/**
	 * The method is called by the query manager to expand the application's query and provide
	 * all possible substitute queries that can replace the initial query and provide the same result
	 * @param queryToExpand: the initial query sent by the application
	 * @param fusionFunctionType: the type of the fusion function that should be applied to data
	 * gathered from services.
	 * @return a final numerical value that is the result of data fusion
	 * @throws UnparsableExpressionException 
	 * @throws UnknownFunctionException 
	 */
	public SensorData expandQuery(Query queryToExpand, String fusionFunctionType, String requestedDataType) throws UnknownFunctionException, UnparsableExpressionException;
	
	

}
