package org.ow2.choreos.queryiotsmanager.interfaces;

import org.ow2.choreos.iots.datatypes.SensorData;
import org.ow2.choreos.query.impl.Query;

/**
 * The Things Query Manager is in charge of recieving and answering queries from
 * applications interested in thing-based services
 * 
 * @author shachem
 * 
 */
public interface ThingsQueryManager {
	/**
	 * getData is the method called to execute and answer the application query
	 * 
	 * @param myQuery
	 *            : the request query
	 * @param fusionFunction
	 *            : the fusion function is a string the determines the type of
	 *            the fusion, e.g., average
	 * @param requestDataType
	 *            : the dataType is a specific data type that extends SensorData
	 * @return the final numerical data value of measurements after fusion
	 * 
	 */
	public SensorData getData(Query myQuery, String requestedDataType,
			String fusionFunction);
	
	public boolean setData(Query myQuery, String requestedDataType,
			String message);

}
