package org.ow2.choreos.queryiotsmanager.impl;


import java.util.HashMap;

import org.ow2.choreos.expansion.impl.ExpansionGeneratorImpl;
import org.ow2.choreos.iots.datatypes.SensorData;
import org.ow2.choreos.query.impl.Query;
import org.ow2.choreos.queryiotsmanager.interfaces.ThingsQueryManager;

import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;




//THE Method calling it is the one that implements the datatype

public class ThingsQueryManagerImpl implements ThingsQueryManager {
 public static String DATATYPE;
 static Query myQuery;
	@Override
	/**
	 * getData is the method called to execute and answer the application query
	 * 
	 * @param myQuery
	 *            : the request query
	 * @param fusionFunction
	 *            : the fusion function is a string the determines the type of
	 *            the fusion, e.g., average
	 * @param requestDataType: the dataType is a specific data type that extends SensorData
	 * @return the final numerical data value of measurements after fusion
	 * 
	 */
	public SensorData getData(Query myQuery,String requestedDataType, String fusionFunction) {
	this.myQuery = myQuery;
		ExpansionGeneratorImpl myExpGen = new ExpansionGeneratorImpl();
		
		if ( myQuery.getConceptsToMeasure().toString().equals("")){
			System.out.println("QueryManager.getData: query concept is empty");
			return null;
		}
		
		if (myQuery.getLocationOfInterest().getLocationName().toString().equals("")){
			System.out.println("QueryManager.getData: query location of interest is empty");
			return null;
		}
		if (fusionFunction.equals("") ){
			System.out.println("QueryManager.getData: fusion fuction is empty");
			return null;
		}
	
		DATATYPE = requestedDataType;
		
//	if	(DATATYPE.equals(Constants.DOUBLE)){
		SensorData result = null;
		try {
			result = myExpGen.expandQuery(myQuery, fusionFunction, requestedDataType);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownFunctionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnparsableExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  result;
	//}
	//else	if	(DATATYPE.equals(Constants.DOUBLE2D)){
	//	DefaultDouble2dData result = (DefaultDouble2dData) myExpGen.expandQuery(myQuery, fusionFunction);
	//	return  result;
	//}


	
	}
	
	public HashMap<String, SensorData> getData(Query myQuery,String requestedDataType) {
	this.myQuery = myQuery;
		ExpansionGeneratorImpl myExpGen = new ExpansionGeneratorImpl();
		
		if ( myQuery.getConceptsToMeasure().toString().equals("")){
			System.out.println("QueryManager.getData: query concept is empty");
			return null;
		}
		
		if (myQuery.getLocationOfInterest().getLocationName().toString().equals("")){
			System.out.println("QueryManager.getData: query location of interest is empty");
			return null;
		}

	
		DATATYPE = requestedDataType;
		
//	if	(DATATYPE.equals(Constants.DOUBLE)){
		HashMap<String, SensorData> result = null;
		try {
			result = myExpGen.expandQuery(myQuery, requestedDataType);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownFunctionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnparsableExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  result;
	//}
	//else	if	(DATATYPE.equals(Constants.DOUBLE2D)){
	//	DefaultDouble2dData result = (DefaultDouble2dData) myExpGen.expandQuery(myQuery, fusionFunction);
	//	return  result;
	//}


	
	}
	
	public String getRequestedDataType(){
		return DATATYPE;
	}
	
	public Query getRequestQuery(){
		return myQuery;
	}

	@Override
	public boolean setData(Query myQuery, String requestedDataType,
			String message) {
		this.myQuery = myQuery;
		ExpansionGeneratorImpl myExpGen = new ExpansionGeneratorImpl();
		
		if ( myQuery.getConceptsToMeasure().toString().equals("")){
			System.out.println("QueryManager.getData: query concept is empty");
			return false;
		}
		
		if (myQuery.getLocationOfInterest().getLocationName().toString().equals("")){
			System.out.println("QueryManager.getData: query location of interest is empty");
			return false;
		}
		if (message.equals("") ){
			System.out.println("QueryManager.getData: message is empty");
			return false;
		}
	
		DATATYPE = requestedDataType;
		
//	if	(DATATYPE.equals(Constants.DOUBLE)){
		boolean result = false;
		try {
			result = myExpGen.sendQuery(myQuery, message, requestedDataType);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  result;
	}


}
