package org.ow2.choreos.lookup.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.lookup.impl.Device;
import org.ow2.choreos.sparqlfunctions.constants.Concept;

public interface LookupManager {

	public Map<Device, Service> findDevices(ArrayList<String> typesOfDevices,
			ArrayList<String> physicalConcepts, Location location,
			double accReq, double covReq);

	// TODO it should look for devices that have services that abstract the
	// types of devices or measure the physical
	// concepts
	/**
	 * This method is used to find registered devices from the device repository
	 * based on an SQL query
	 * 
	 * @param queryExpression
	 *            : the SQL query
	 * @return a list of services hosted by devices that satisfy the query
	 *         requirements
	 * @throws SQLException
	 */
	public ArrayList<Service> findDevices(String queryExpression)
			throws SQLException;

	/**
	 * This method is used to find registered devices from the device repository
	 * based on the concepts that services they host measure and the location
	 * they are at
	 * 
	 * @param physicalConcepts
	 *            : the concepts of interest that should be measured
	 * @param location
	 *            : the location of interest at which devices they should be
	 * @return a list of services hosted by devices that measure the concepts of
	 *         interest at the location of interest
	 */
	public ArrayList<Service> findDevices(ArrayList<Concept> physicalConcepts,
			Location location);
	public ArrayList<Service> findDevices(Concept physicalConcepts,
			Location location);
	public ArrayList<Service> findSubSetOfDevices(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept, double eventOfInterstXCoords,
			double eventOfInterestYCoords, String distribution,
			int subSetSize);

	public ArrayList<Service> findSubSetOfDevices(
			ArrayList<Concept> expandedConcepts, Location locationOfInterest,
			String distribution, int subSetSize);

	ArrayList<Service> findDevicesInSubRegion(
			ArrayList<Concept> expandedConcepts, Location location);

	ArrayList<Service> findSubSetOfDevicesBasedOnCoverage(ArrayList<Concept> expandedConcepts,
			Location locationOfInterst, String distribution, double c, long time);

	ArrayList<Service> findSubSetOfDevicesBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept, double eventOfInterstXCoords,
			double eventOfInterestYCoords, String distribution, double c, long time
			);

}
