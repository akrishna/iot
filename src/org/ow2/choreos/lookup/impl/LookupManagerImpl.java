package org.ow2.choreos.lookup.impl;

import java.sql.SQLException;

import java.util.ArrayList;

import java.util.Map;

import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.lookup.interfaces.LookupManager;
import org.ow2.choreos.queryregistryclient.impl.RegistryClientManager;
import org.ow2.choreos.queryregistryclient.interfaces.RegistryClient;
import org.ow2.choreos.sparqlfunctions.constants.Concept;

public class LookupManagerImpl implements LookupManager {
static String registryAddress = "http://localhost:8080/RegistryManager/rest/";

//static String registryAddress =
//"http://node010:9090/RegistryManager-1.0/rest/";

	@Override
	/**
	 * This method is used to find registered devices from the device repository based on the concepts that services
	 * they host measure and the location they are at
	 * @param physicalConcepts: the concepts of interest that should be measured
	 * @param location: the location of interest at which devices they should be
	 * @return a list of services hosted by devices that measure the concepts of interest at the location
	 * of interest
	 */
	// TODO the method should no longer look for devices based on dataType but
	// only based on concept,
	// datatype is kept in the query but in here it shouldnt be part of the
	// criteria
	public ArrayList<Service> findDevices(ArrayList<Concept> physicalConcepts,
			Location location) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReq(physicalConcepts,
					location);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findDevices: no registry found");
			return null;
		}

		return myFoundServices;

	}

	@Override
	public Map<Device, Service> findDevices(ArrayList<String> typesOfDevices,
			ArrayList<String> physicalConcepts, Location locationToCheck,
			double accReq, double covReq) {
		// TO CREATE DEVICE OBJECTS WITH THE ACCESS ADDRESSES FOR THEIR SERVICES
		return null;
	}

	/**
	 * This method is used to find registered devices from the device repository
	 * based on an SQL query
	 * 
	 * @param queryExpression
	 *            : the SQL query
	 * @return a list of services hosted by devices that satisfy the query
	 *         requirements
	 * @throws SQLException
	 */
	public ArrayList<Service> findDevices(String query) throws SQLException {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReq(query);
		}

		catch (NullPointerException e) {
			System.out
					.println("lookupManagerImpl.findDevices : no registry found");
			return null;
		}

		return myFoundServices;

	}

	@Override
	public ArrayList<Service> findSubSetOfDevices(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept, double eventOfInterstXCoords,
			double eventOfInterestYCoords, String distribution, int subSetSize) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReq(expandedConcepts,
					location, distancePerConcept, eventOfInterstXCoords,
					eventOfInterestYCoords, distribution, subSetSize);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findDevices: no registry found");
			return null;
		}

		return myFoundServices;

	}
	
	@Override
	public ArrayList<Service> findSubSetOfDevicesBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept, double eventOfInterstXCoords,
			double eventOfInterestYCoords, String distribution, double c, long time) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReqBasedOnCoverage(expandedConcepts,
					location, distancePerConcept, eventOfInterstXCoords,
					eventOfInterestYCoords, distribution, c, time);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findSubSetOfDevicesBasedOnCoverage: no registry found");
			return null;
		}

		return myFoundServices;

	}

	@Override
	public ArrayList<Service> findSubSetOfDevices(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, int subSetSize) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReq(expandedConcepts,
					locationOfInterst, distribution, subSetSize);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findDevices: no registry found");
			return null;
		}

		return myFoundServices;
	}
	

	@Override
	public ArrayList<Service> findSubSetOfDevicesBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, double c , long time) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReqBasedOnCoverage(expandedConcepts, locationOfInterst, distribution, c , time);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findSubSetOfDevicesBasedOnCoverage: no registry found");
			return null;
		}

		return myFoundServices;
	}
	
	public void findSubSetOfDevicesBasedOnCoverageSim(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, double c , long time) {
	String myFoundServices ;

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

		myReg.executeLookupQueryReqBasedOnCoverageSim(expandedConcepts, locationOfInterst, distribution, c , time);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findSubSetOfDevicesBasedOnCoverageSim: error in getting result from registry");
		
		}


	}

	@Override
	public ArrayList<Service> findDevices(Concept physicalConcept,
			Location location) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReq(physicalConcept,
					location);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findDevices: no registry found");
			return null;
		}

		return myFoundServices;
	}

	@Override
	public ArrayList<Service> findDevicesInSubRegion(
			ArrayList<Concept> expandedConcepts, Location location) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();

		try {
			RegistryClient myReg = RegistryClientManager
					.getRegistry(registryAddress);

			myFoundServices = myReg.executeLookupQueryReqInSubRegion(
					expandedConcepts, location);
		}

		catch (NullPointerException e) {
			System.out
					.println("LookupManagerImpl.findDevicesInSubRegion: no registry found");
			return null;
		}

		return myFoundServices;
	}
}
