package org.ow2.choreos.lookup.impl;

public class Device {
	
	String myDeviceType;
	int myDeviceId;
	public Device(String deviceType, int deviceId){
		myDeviceType = deviceType;
		myDeviceId = deviceId;
	}

public String getDeviceType(){
	return myDeviceType;
}

public String getDeviceLocation(){
	return "";
}


public int getDeviceId(){
	return myDeviceId;
}


}
