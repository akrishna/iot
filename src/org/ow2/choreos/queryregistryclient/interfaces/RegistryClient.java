package org.ow2.choreos.queryregistryclient.interfaces;

import java.util.ArrayList;

import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Point;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.sparqlfunctions.constants.Concept;

public interface RegistryClient {

	/**
	 * This methods executes an SQL query (either locally or by invoking a Web
	 * service) over a database (that can be remote) that should return a list
	 * of services that satisfy the requirements of the query.
	 * 
	 * @param query
	 *            : an SQL query
	 * @return a list of services that result from the query execution
	 */
	public ArrayList<Service> executeLookupQueryReq(String query);

	/**
	 * This method invokes a Web Service that allows the middleware to access
	 * the things registry where devices store their services, in order to find
	 * the services that measure the physical concepts of interest and are at
	 * the location of interest
	 * 
	 * @param physicalConcepts
	 *            : the concept of interest that need to be measured
	 * @param location
	 *            : the location at which measurements should take place
	 * @return a list of services that satisfy the requirements
	 */
	public ArrayList<Service> executeLookupQueryReq(
			ArrayList<Concept> physicalConcepts, Location location);
	public ArrayList<Service> executeLookupQueryReq(
			Concept physicalConcept, Location location);
	/**
	 * The method invokes a Web service that discovers a subset of devices that
	 * can measure a concept around a point of interest
	 * 
	 * @param expandedConcepts
	 * @param distancePerConcept
	 * @param eventOfInterstXCoords
	 * @param eventOfInterestYCoords
	 * @param distribution
	 * @param subSetSize
	 * @return
	 */
	public ArrayList<Service> executeLookupQueryReq(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept, double eventOfInterstXCoords,
			double eventOfInterestYCoords, String distribution,
			int subSetSize);

	/**
	 * The method invokes a web service that discovers a subset of devices that
	 * can measure a concept in an area of interest
	 * 
	 * @param expandedConcepts
	 * @param locationOfInterst
	 * @param distribution
	 * @param subSetSize
	 * @return
	 */
	public ArrayList<Service> executeLookupQueryReq(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, int subSetSize);


	ArrayList<Service> executeLookupQueryReqInSubRegion(
			ArrayList<Concept> expandedConcepts, Location locationOfInterest);

	ArrayList<Service> executeLookupQueryReqBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept,
			double eventOfInterestXCoords, double eventOfInterestYCoords,
			String distribution, double c, long time);

	ArrayList<Service> executeLookupQueryReqBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, double c, long time);

	void executeLookupQueryReqBasedOnCoverageSim(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, double c, long time);

}
