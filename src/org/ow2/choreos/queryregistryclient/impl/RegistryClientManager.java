package org.ow2.choreos.queryregistryclient.impl;

import java.util.Hashtable;
import java.util.Map;

import org.ow2.choreos.queryregistryclient.interfaces.RegistryClient;

public class RegistryClientManager {

	static Map<String, RegistryClient> singleTonRegistryMap = new Hashtable<String, RegistryClient>();

	static {

		singleTonRegistryMap.put(
				"http://node010:9090/RegistryManager-1.0/rest/",
				new RestRegistryImpl(
						"http://node010:9090/RegistryManager-1.0/rest/"));
		singleTonRegistryMap
				.put("http://things.vtrip.net:8080/RegistryManager-2.1/rest/",
						new RestRegistryImpl(
								"http://things.vtrip.net:8080/RegistryManager-2.1/rest/"));

		singleTonRegistryMap
				.put("http://things.vtrip.net:8080/RegistryManager-7.0/rest/",
						new RestRegistryImpl(
								"http://things.vtrip.net:8080/RegistryManager-7.0/rest/"));
		singleTonRegistryMap
				.put("http://things.vtrip.net:8080/RegistryManager-8.0/rest/",
						new RestRegistryImpl(
								"http://things.vtrip.net:8080/RegistryManager-8.0/rest/"));
		singleTonRegistryMap
				.put("http://things.vtrip.net:8080/RegistryManager-9.0/rest/",
						new RestRegistryImpl(
								"http://things.vtrip.net:8080/RegistryManager-9.0/rest/"));
		singleTonRegistryMap
				.put("http://things.vtrip.net:8080/RegistryManager-10.0/rest/",
						new RestRegistryImpl(
								"http://things.vtrip.net:8080/RegistryManager-10.0/rest/"));

		singleTonRegistryMap.put("http://localhost:8080/RegistryManager/rest/",
				new RestRegistryImpl(
						"http://localhost:8080/RegistryManager/rest/"));
		singleTonRegistryMap
				.put("http://arles.rocq.inria.fr:8080/RegistryManager/rest/",
						new RestRegistryImpl(
								"http://arles.rocq.inria.fr:8080/RegistryManager/rest/"));

	}

	public static RegistryClient getRegistry(String registryAddress) {
		// TODO Auto-generated method stub

		return singleTonRegistryMap.get(registryAddress);

	}

}
