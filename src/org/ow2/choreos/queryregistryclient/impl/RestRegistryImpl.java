package org.ow2.choreos.queryregistryclient.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Point;
import org.ow2.chereos.iots.common.Service;
import org.ow2.chereos.iots.common.ServiceDescription;
import org.ow2.choreos.distributions.Constants;
import org.ow2.choreos.queryregistryclient.interfaces.RegistryClient;
import org.ow2.choreos.sparqlfunctions.constants.Concept;

public class RestRegistryImpl implements RegistryClient {
	String baseURL;

	public RestRegistryImpl(String urlString) {
		baseURL = urlString;
	}

	@Override
	/**
	 * This methods invokes a Web service over a remote database that should execute an SQL query and
	 * return a list of services that satisfy the requirements of the query. 
	 * @param query: an SQL query
	 * @return a list of services that result from the query execution
	 */
	public ArrayList<Service> executeLookupQueryReq(String query) {
		return null;

	}

	@Override
	/**
	 * This methods executes invokes a Web Service that allows the middleware to access the things registry
	 * where devices store their services, in order to find the services that measure the physical concepts of
	 * interest and are at the location of  interest
	 * @param physicalConcepts: the concept of interest that need to be measured
	 * @param location: the location at which measurements should take place
	 * @return a list of services that satisfy the requirements
	 */
	public ArrayList<Service> executeLookupQueryReq(
			ArrayList<Concept> physicalConcepts, Location location) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		HashMap<String, Double> thisMap = new HashMap<String, Double>();
		String paramString = "?global_region=" + location.getLocationName();
		for (int i = 0; i < physicalConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ physicalConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query" + paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReq : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReq : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;
	}

	@Override
	public ArrayList<Service> executeLookupQueryReq(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept,
			double eventOfInterestXCoords, double eventOfInterestYCoords,
			String distribution, int subSetSize) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		String paramString = "?global_region=" + location.getLocationName();
		paramString = paramString + "&point_of_interest_x="
				+ eventOfInterestXCoords;
		paramString = paramString + "&point_of_interest_y="
				+ eventOfInterestYCoords;
		paramString = paramString + "&distribution=" + distribution;
		for (int i = 0; i < distancePerConcept.size(); i++) {
			paramString = paramString + "&distance="
					+ distancePerConcept.get(i);

		}
		paramString = paramString + "&subset_size=" + subSetSize;
		for (int i = 0; i < expandedConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ expandedConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query_around_point"
					+ paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReq : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReq : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;
	}

	@Override
	public ArrayList<Service> executeLookupQueryReqBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location location,
			ArrayList<Double> distancePerConcept,
			double eventOfInterestXCoords, double eventOfInterestYCoords,
			String distribution, double c, long time) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		String paramString = "?global_region=" + location.getLocationName();
		paramString = paramString + "&point_of_interest_x="
				+ eventOfInterestXCoords;
		paramString = paramString + "&point_of_interest_y="
				+ eventOfInterestYCoords;
		paramString = paramString + "&distribution=" + distribution;
		for (int i = 0; i < distancePerConcept.size(); i++) {
			paramString = paramString + "&distance="
					+ distancePerConcept.get(i);

		}
		paramString = paramString + "&coverage=" + c;

		paramString = paramString + "&time=" + time;

		for (int i = 0; i < expandedConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ expandedConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query_around_point"
					+ paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReqBasedOnCoverage : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReqBasedOnCoverage : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;
	}

	@Override
	public ArrayList<Service> executeLookupQueryReq(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, int subSetSize) {

		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		String paramString = "?global_region="
				+ locationOfInterst.getLocationName();
		paramString = paramString
				+ "&bottom_left_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getBottomLeftPoint()
						.getX();
		paramString = paramString
				+ "&bottom_left_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getBottomLeftPoint()
						.getY();
		paramString = paramString
				+ "&bottom_right_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getBottomRightPoint()
						.getX();
		paramString = paramString
				+ "&bottom_right_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getBottomRightPoint()
						.getY();
		paramString = paramString
				+ "&upper_left_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getUpperLeftPoint()
						.getX();
		paramString = paramString
				+ "&upper_left_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getUpperLeftPoint()
						.getY();
		paramString = paramString
				+ "&upper_right_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getUpperRightPoint()
						.getX();
		paramString = paramString
				+ "&upper_right_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getUpperRightPoint()
						.getY();

		paramString = paramString + "&distribution=" + distribution;

		paramString = paramString + "&subset_size=" + subSetSize;
		for (int i = 0; i < expandedConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ expandedConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query_in_area"
					+ paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReq : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReq : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;

	}

	@Override
	public ArrayList<Service> executeLookupQueryReqBasedOnCoverage(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, double c, long time) {

		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		String paramString = "?global_region="
				+ locationOfInterst.getLocationName();
		paramString = paramString
				+ "&bottom_left_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getBottomLeftPoint()
						.getX();
		paramString = paramString
				+ "&bottom_left_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getBottomLeftPoint()
						.getY();
		paramString = paramString
				+ "&bottom_right_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getBottomRightPoint()
						.getX();
		paramString = paramString
				+ "&bottom_right_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getBottomRightPoint()
						.getY();
		paramString = paramString
				+ "&upper_left_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getUpperLeftPoint()
						.getX();
		paramString = paramString
				+ "&upper_left_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getUpperLeftPoint()
						.getY();
		paramString = paramString
				+ "&upper_right_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getUpperRightPoint()
						.getX();
		paramString = paramString
				+ "&upper_right_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getUpperRightPoint()
						.getY();

		paramString = paramString + "&distribution=" + distribution;

		paramString = paramString + "&coverage=" + c;
		paramString = paramString + "&time=" + time;

		for (int i = 0; i < expandedConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ expandedConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query_in_area"
					+ paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReq : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReq : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;

	}
@Override
	public void executeLookupQueryReqBasedOnCoverageSim(
			ArrayList<Concept> expandedConcepts, Location locationOfInterst,
			String distribution, double c, long time) {

		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		String paramString = "?global_region="
				+ locationOfInterst.getLocationName();
		paramString = paramString
				+ "&bottom_left_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getBottomLeftPoint()
						.getX();
		paramString = paramString
				+ "&bottom_left_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getBottomLeftPoint()
						.getY();
		paramString = paramString
				+ "&bottom_right_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getBottomRightPoint()
						.getX();
		paramString = paramString
				+ "&bottom_right_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getBottomRightPoint()
						.getY();
		paramString = paramString
				+ "&upper_left_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getUpperLeftPoint()
						.getX();
		paramString = paramString
				+ "&upper_left_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getUpperLeftPoint()
						.getY();
		paramString = paramString
				+ "&upper_right_point_of_interest_x="
				+ locationOfInterst.getBorderPoints().getUpperRightPoint()
						.getX();
		paramString = paramString
				+ "&upper_right_point_of_interest_y="
				+ locationOfInterst.getBorderPoints().getUpperRightPoint()
						.getY();

		paramString = paramString + "&distribution=" + distribution;

		paramString = paramString + "&coverage=" + c;
		paramString = paramString + "&time=" + time;

		for (int i = 0; i < expandedConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ expandedConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query_in_area"
					+ paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				System.out.println("RestRegistryImpl.executeLookupQueryReqBasedOnCoverageSim: sent lookup request to registry");
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReqBasedOnCoverageSim : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					String[] responseArr = response.split(",");
					System.out.println("response is ---> " + response);

					FileWriter fstream = new FileWriter("/home/rioc/shachem/"
							+ "queryTimer" + "_" + c + ".csv", true);

				//	FileWriter fstream = new FileWriter("/Users/shachem/Desktop/"
				//			+ "queryTimer" + "_" + c + ".csv", true);
					BufferedWriter outWriter = new BufferedWriter(fstream);
					String timeString = response + "\n";

					outWriter.write(timeString);

					outWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("IO error in executeLookupReqSim");
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public ArrayList<Service> executeLookupQueryReqInSubRegion(
			ArrayList<Concept> expandedConcepts, Location locationOfInterest) {

		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		String paramString = "?global_region="
				+ locationOfInterest.getLocationName();
		paramString = paramString + "&bottom_left_point_of_interest_x="
				+ locationOfInterest.getBottomLeft().getX();
		paramString = paramString + "&bottom_left_point_of_interest_y="
				+ locationOfInterest.getBottomLeft().getY();

		paramString = paramString + "&upper_right_point_of_interest_x="
				+ locationOfInterest.getUpperRight().getX();
		paramString = paramString + "&upper_right_point_of_interest_y="
				+ locationOfInterest.getUpperRight().getY();
		for (int i = 0; i < expandedConcepts.size(); i++) {
			paramString = paramString + "&concept="
					+ expandedConcepts.get(i).getConceptName();

		}

		try {
			URL url = new URL(baseURL + "execute_lookup_query_in_subregion"
					+ paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReqInSubRegion : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReqInSubRegion : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;

	}

	@Override
	public ArrayList<Service> executeLookupQueryReq(Concept physicalConcept,
			Location location) {
		ArrayList<Service> myFoundServices = new ArrayList<Service>();
		HashMap<String, Double> thisMap = new HashMap<String, Double>();
		String paramString = "?global_region=" + location.getLocationName();

		paramString = paramString + "&concept="
				+ physicalConcept.getConceptName();

		try {
			URL url = new URL(baseURL + "execute_lookup_query" + paramString);
			HttpURLConnection conn = null;

			try {
				conn = (HttpURLConnection) url.openConnection();
				// conn.setDoOutput(true);
			} catch (IOException e) {
				System.out
						.println("RestRegistryImpl.executeLookupQueryReq : the connection was refused");
				// e.printStackTrace();
			}

			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			String response = null;
			try {

				while ((response = in.readLine()) != null) {
					JSONParser myParser = new JSONParser();
					try {
						JSONObject responseObject = (JSONObject) myParser
								.parse(response);
						System.out.println(responseObject);

						JSONArray thisarr = (JSONArray) responseObject
								.get("value");
						if (thisarr != null) {
							for (Object j : thisarr) {
								ServiceDescription s = ServiceDescription
										.deserializeFromJSONObject((JSONObject) j);
								Service newService = new Service(
										s.getDeviceID(), s.getName(),
										s.getType(), s.getAccuracy(),
										s.getSensor(), s.getConcept(),
										s.getRange(), 0.0, s.getAddress(),
										s.getDataType(), s.getLocationX(),
										s.getLocationY(), s.getUnit());
								myFoundServices.add(newService);

							}
						} else
							System.out
									.println(" RestRegistryImpl.executeLookupQueryReq : no results found");
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return myFoundServices;

	}
}
