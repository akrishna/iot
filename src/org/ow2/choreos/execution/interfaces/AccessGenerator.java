package org.ow2.choreos.execution.interfaces;

import java.util.ArrayList;
import java.util.HashMap;

import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.iots.datatypes.SensorData;



public interface AccessGenerator {

	/**
	 * This method is called to access devices providing services based on the
	 * address of services and get their measurements. It actually invokes a web service hosted 
	 * on the devices to access
	 * 
	 * @param services: a list of services to access
	 * @return a HashMap containing the address of each service and the value of its measurement
	 */
	HashMap<String, HashMap<SensorData, Location>> AccessDevice(ArrayList<Service> services);

}
