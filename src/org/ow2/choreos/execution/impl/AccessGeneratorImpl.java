package org.ow2.choreos.execution.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ow2.chereos.iots.common.Constants;
import org.ow2.chereos.iots.common.DataDescription;
import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.execution.interfaces.AccessGenerator;
import org.ow2.choreos.iots.datatypes.DefaultBooleanData;
import org.ow2.choreos.iots.datatypes.DefaultDouble2dData;
import org.ow2.choreos.iots.datatypes.DefaultDouble2dLocationData;
import org.ow2.choreos.iots.datatypes.DefaultDouble3dData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleNoiseLevelData;
import org.ow2.choreos.iots.datatypes.DefaultDoublePressurePaData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleSoundData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleTemperatureCelsiusData;
import org.ow2.choreos.iots.datatypes.DefaultDoubleTemperatureFahrenheitData;
import org.ow2.choreos.iots.datatypes.DefaultIntegerData;
import org.ow2.choreos.iots.datatypes.DefaultLongData;
import org.ow2.choreos.iots.datatypes.SensorData;

public class AccessGeneratorImpl implements AccessGenerator {
	// TODO for now this method will access a webservice that stores sensor
	// readings
	// instead of accessing devices directly

	@Override
	// HOW DO WE KNOW THE TYPE OF DATA TO RETURN?
	// SHOULD I CREATE A DATA CLASS THAT HAS A CONSTRUCTOR FOR ANY POSSIBLE
	// OPTION? WHAT IF DEVICE MEASURES SEVERAL CONCEPTS?
	// TODO: one access request per device, not one that has an array of devices
	/**
	 * This method is called to access devices providing services based on the
	 * address of services and get their measurements. It actually invokes a web service hosted 
	 * on the devices to access
	 * 
	 * @param services: a list of services to access
	 * @return a HashMap containing the address of each service and the value of its measurement
	 */
	// TODO this double should be a generic datatype that can be a double or a
	// location, etc.
	// public HashMap<String, SensorData> AccessDevice(
	// ArrayList<Service> servicesToAccess) {
	// HashMap<String, SensorData> thisMap = new HashMap<String, SensorData>();
	// if (servicesToAccess.size() == 0) {
	// System.out
	// .println("AccessGeneratorImpl.AccessDevice: there are no services to access");
	// return null;
	// }
	//
	// HashMap<String, SensorData> finalMap = new HashMap<String, SensorData>();
	//
	//
	// String paramString = "";
	// for (int i = 0; i < servicesToAccess.size(); i++)
	// try {
	// paramString = "?address="
	// + servicesToAccess.get(i).getServiceAddress();
	// URL url = new URL(
	// "http://things.vtrip.net:8080/registry_manager/get_data"
	// + paramString);
	// URLConnection conn = null;
	// try {
	// conn = url.openConnection();
	// conn.setDoOutput(true);
	// } catch (IOException e) {
	// System.out.println("AccessGeneratorImpl.AccessDevice: the connection was refused");
	// e.printStackTrace();
	// return null;
	//
	// }
	//
	// BufferedReader in = null;
	// try {
	// in = new BufferedReader(new InputStreamReader(
	// conn.getInputStream()));
	// } catch (IOException e) {
	// System.out
	// .println("AccessGeneratorImpl.AccessDevice: could not get inputStream");
	// e.printStackTrace();
	// return null;
	// }
	//
	// String response = null;
	// try {
	// while ((response = in.readLine()) != null) {
	// JSONParser myParser = new JSONParser();
	// try {
	// JSONObject responseObject = (JSONObject) myParser
	// .parse(response);
	// System.out.println(responseObject);
	//
	// JSONArray thisarr = (JSONArray) responseObject
	// .get("value");
	// if (thisarr != null) {
	// for (Object j : thisarr) {
	// if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.DOUBLE2D)) {
	//
	// SensorData s = new DefaultDouble2dData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// } else if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.DOUBLE)) {
	//
	// SensorData s = new DefaultDoubleData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// }
	// else if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.DOUBLE2DLOCATION)) {
	//
	// SensorData s = new DefaultDouble2dLocationData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// }
	//
	// else if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.DOUBLE3D)) {
	//
	// SensorData s = new DefaultDouble3dData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// }
	// else if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.INTEGER)) {
	//
	// SensorData s = new DefaultIntegerData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// } else if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.LONG)) {
	//
	// SensorData s = new DefaultLongData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// }
	//
	// else if (ThingsQueryManagerImpl.DATATYPE
	// .equals(Constants.LONGNOISE)) {
	//
	// SensorData s = new DefaultLongNoiseLevelData(
	// (JSONObject) j);
	// finalMap.put(servicesToAccess.get(i).getServiceAddress(), s);
	//
	// }
	//
	//
	//
	// }
	// }
	// }
	//
	// catch (ParseException e) {
	// e.printStackTrace();
	// return null;
	// }
	//
	// }
	// } catch (IOException e) {
	// e.printStackTrace();
	// return null;
	// }
	// try {
	// in.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// return null;
	// }
	//
	// } catch (MalformedURLException e) {
	// e.printStackTrace();
	// }
	// /*
	// * if (counter == (int) j * 100) {
	// *
	// * initVal = counter; j = servicesToAccess.size(); factor = 1; }
	// */
	// // }
	// //System.out.println("final map is:" + finalMap);
	// return finalMap;
	//
	// }
	/**
	 * This method is called to access devices providing services based on the
	 * address of services and get their measurements. It actually invokes a web service hosted 
	 * on a local machine
	 * 
	 * @param servicesToAccess: a list of services to access
	 * @return a HashMap containing the address of each service and the value of its measurement
	 */
	public HashMap<String, HashMap<SensorData, Location>> AccessDevice(
			ArrayList<Service> servicesToAccess) {
		HashMap<String, HashMap<SensorData, Location>> finalMap = new HashMap<String, HashMap<SensorData, Location>>();
		HashMap<SensorData, Location> dataLocMap = new HashMap<SensorData, Location>();
		if (servicesToAccess.size() == 0) {
			System.out
					.println("AccessGeneratorImpl.AccessDevice: there are no services to access");
			return null;
		}

		String paramString = "";
		for (int i = 0; i < servicesToAccess.size(); i++)
			try {

				paramString = servicesToAccess.get(i).getServiceAddress();
				URL url = new URL(paramString);
				HttpURLConnection conn = null;
				try {
					conn = (HttpURLConnection) url.openConnection();
					// conn.setDoOutput(true);
				} catch (IOException e) {
					System.out.println("the connection was refused");
					e.printStackTrace();
					return null;

					// String paramString = "";
					// for (int i = 0; i < servicesToAccess.size(); i++)
					// try {
					// paramString = "?address="
					// + servicesToAccess.get(i).getServiceAddress();
					// URL url = new URL(
					// "http://localhost:8080/RegistryManager/get_data"
					// + paramString);
					// URLConnection conn = null;
					// try {
					// conn = url.openConnection();
					// conn.setDoOutput(true);
					// } catch (IOException e) {
					// System.out
					// .println("AccessGeneratorImpl.AccessDevice: the connection was refused");
					// e.printStackTrace();
					// return null;

				}

				BufferedReader in = null;
				try {
					in = new BufferedReader(new InputStreamReader(
							conn.getInputStream()));
				} catch (IOException e) {
					System.out
							.println("AccessGeneratorImpl.AccessDevice: could not get inputStream");
					e.printStackTrace();
					return null;
				}

				String response = null;
				try {
					// TODO ERROR I NEED TO CREATE THE SENSOR OBJECT BASED ON
					// THE SENT DATATYOE NOT REQUESTED DATA TYPE
					// COMPUTE METHOD IS EMPTY CHECK Y
					while ((response = in.readLine()) != null) {
						JSONParser myParser = new JSONParser();
						try {
							JSONObject responseObject = (JSONObject) myParser
									.parse(response);
							System.out.println(responseObject);

							// JSONArray thisarr = (JSONArray) responseObject
							// .get("sensorDataResponse");
							JSONObject sensorDataResponceObject = null;
							sensorDataResponceObject = (JSONObject) responseObject
									.get("sensorDataResponse");

							if (sensorDataResponceObject == null)
								continue;
							// I commented this out while working on the
							// citymoov demo because apparently theere is no
							// array to return, it is only one json object
							// because it s one result per device
							// if (thisarr != null) {
							// for (Object j : thisarr) {
							//
							// DataDescription d = DataDescription
							// .deserializeFromJSONObject((JSONObject) j);
							//
							// if (d.getDataType().equals(
							// Constants.DOUBLE2D)) {
							//
							// SensorData s = new DefaultDouble2dData(
							// d.getDataDouble1(),
							// d.getDataDouble2(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.DOUBLE)) {
							//
							// SensorData s = new DefaultDoubleData(
							// d.getdataDouble(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.DOUBLE2DLOCATION)) {
							//
							// SensorData s = new DefaultDouble2dLocationData(
							// d.getDataDouble1(),
							// d.getDataDouble2(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// }
							//
							// else if (d.getDataType().equals(
							// Constants.DOUBLE3D)) {
							// SensorData s = new DefaultDouble3dData(
							// d.getDataDouble1(),
							// d.getDataDouble2(),
							// d.getDataDouble3(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.INTEGER)) {
							// SensorData s = new DefaultIntegerData(
							// d.getDataInteger(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.LONG)) {
							// SensorData s = new DefaultLongData(
							// d.getDataLong(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// }
							//
							// else if (d.getDataType().equals(
							// Constants.DOUBLENOISE)) {
							//
							// SensorData s = new DefaultDoubleNoiseLevelData(
							// d.getdataDouble(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// }
							//
							// else if (d.getDataType().equals(
							// Constants.DOUBLESOUND)) {
							//
							// SensorData s = new DefaultDoubleSoundData(
							// d.getdataDouble(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.DOUBLETEMPERATUREC)) {
							//
							// SensorData s = new
							// DefaultDoubleTemperatureCelsiusData(
							// d.getdataDouble(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.BOOLEAN)) {
							//
							// SensorData s = new DefaultBooleanData(
							// d.getdataBoolean(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.DOUBLETEMPERATUREF)) {
							//
							// SensorData s = new
							// DefaultDoubleTemperatureFahrenheitData(
							// d.getdataDouble(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// } else if (d.getDataType().equals(
							// Constants.DOUBLEPRESSUREPA)) {
							//
							// SensorData s = new DefaultDoublePressurePaData(
							// d.getdataDouble(),
							// new Date().getTime());
							// dataLocMap = new HashMap();
							// dataLocMap.put(s,
							// new Location("", d.getXCoord(),
							// d.getYCoord()));
							// finalMap.put(servicesToAccess.get(i)
							// .getServiceAddress(),
							// dataLocMap);
							//
							// }
							//
							// }
							// }

							String dataType = (String) sensorDataResponceObject
									.get("dataType");

							System.out.println(dataType);

							if (dataType.equals(Constants.DOUBLENOISE)) {

								double value = Double
										.valueOf((String) sensorDataResponceObject
												.get("value"));
								double xCoord = 0;
								double yCoord = 0;

								SensorData s = new DefaultDoubleNoiseLevelData(
										value, new Date().getTime());
								dataLocMap = new HashMap();
								dataLocMap.put(s, new Location("", xCoord,
										yCoord));
								finalMap.put(servicesToAccess.get(i)
										.getServiceAddress(), dataLocMap);

							} else if (dataType
									.equals("org.ow2.choreos.sensordata.4d-double.location")) {
								double longitude = Double
										.valueOf((String) sensorDataResponceObject
												.get("longitude"));
								double latitude = Double
										.valueOf((String) sensorDataResponceObject
												.get("latitude"));

								SensorData s = new DefaultDouble2dLocationData(
										latitude, longitude,
										new Date().getTime());
								dataLocMap = new HashMap();
								dataLocMap.put(s, new Location("", latitude,
										longitude));
								finalMap.put(servicesToAccess.get(i)
										.getServiceAddress(), dataLocMap);
							} else {

							}
						}

						catch (ParseException e) {
							e.printStackTrace();
							return null;
						}

					}
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		/*
		 * if (counter == (int) j * 100) {
		 * 
		 * initVal = counter; j = servicesToAccess.size(); factor = 1; }
		 */
		// }
		// System.out.println("final map is:" + finalMap);
		return finalMap;

	}

	public boolean AccessDevice(ArrayList<Service> servicesToAccess,
			String message) {
		HashMap<String, HashMap<SensorData, Location>> finalMap = new HashMap<String, HashMap<SensorData, Location>>();
		HashMap<SensorData, Location> dataLocMap = new HashMap<SensorData, Location>();
		boolean result;
		if (servicesToAccess.size() == 0) {
			System.out
					.println("AccessGeneratorImpl.AccessDevice: there are no services to access");
			return false;
		}

		String paramString = "";
		for (int i = 0; i < servicesToAccess.size(); i++)
			try {
				paramString = servicesToAccess.get(i).getServiceAddress();
				URL url = new URL(paramString);
				URLConnection conn = null;
				try {
					conn = url.openConnection();
					conn.setDoOutput(true);
				} catch (IOException e) {
					System.out.println("the connection was refused");
					e.printStackTrace();
					return false;

				}
				/*
				 * String paramString = ""; for (int i = 0; i <
				 * servicesToAccess.size(); i++) try { paramString = "?address="
				 * + servicesToAccess.get(i).getServiceAddress(); URL url = new
				 * URL( "http://localhost:8080/RegistryManager/set_data" +
				 * paramString); URLConnection conn = null;
				 * 
				 * try { conn = url.openConnection(); } catch (IOException e) {
				 * // TODO Auto-generated catch block
				 * System.out.println("the connection was refused");
				 * e.printStackTrace(); return false; }
				 */
				conn.setDoOutput(true);

				BufferedWriter out = null;
				try {
					out = new BufferedWriter(new OutputStreamWriter(
							conn.getOutputStream()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				try {

					out.write("message=" + message);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				try {
					out.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				BufferedReader in = null;
				try {
					in = new BufferedReader(new InputStreamReader(
							conn.getInputStream()));
					try {
						String response = "";

						while ((response = in.readLine()) != null) {
							if (response.equals("true"))
								result = true;
							else
								result = false;
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return false;
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return false;
					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		return true;
	}
}
