package org.ow2.choreos.execution.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.execution.interfaces.ExecutionGenerator;
import org.ow2.choreos.iots.datatypes.SensorData;
import org.ow2.choreos.query.impl.SubQuery;
import org.ow2.choreos.sparqlfunctions.constants.DataFusionGeneratorImpl;
import org.ow2.choreos.sparqlfunctions.constants.Formula;


public class ExecutionGeneratorImpl implements ExecutionGenerator {

	@Override
	/**
	 * This method is called by the expansion manager to request measurements provided by services based 
	 * on expanded queries and their corresponding functions (if any)
	 * @param servicesToAccess: the list of services to access
	 * @param queriesAndFunctions: the substitute queries that can replace the initial query and their corresponding
	 * mathematical functions that will be used to compute the concepts of interest
	 * @return a HashMap containing the address of the service and its measurement. It should be noted that the method
	 * returns one measurement per service, as for the moment we do not support historical data
	 */
	public HashMap<String, HashMap<SensorData,Location>> getValues(
			ArrayList<Service> servicesToAccess
			) {
		HashMap<String, HashMap<SensorData,Location>> dataFromDevices = new HashMap<String, HashMap<SensorData,Location>>();
		AccessGeneratorImpl ag = new AccessGeneratorImpl();
		DataFusionGeneratorImpl dg = new DataFusionGeneratorImpl();
		ArrayList<Double> data = new ArrayList<Double>();
		try {
			HashMap<String, HashMap<SensorData,Location>> dataMap = ag
					.AccessDevice(servicesToAccess);
		//	if ( ThingsQueryManagerImpl.DATATYPE.equals("Double")){
				
		//	for (Entry<String, SensorData> entry : dataMap
		//			.entrySet()) {
		//		DefaultDoubleData doubleDataVal = (DefaultDoubleData) entry.getValue();
		//		dataFromDevices.put(entry.getKey(), doubleDataVal); }
		//	}
			dataFromDevices = dataMap;	
		} catch (NullPointerException e) {
			System.out
					.println("ExecutionGeneratorImpl.getValues: no devices to access");
			return null;
		}

		// TODO Auto-generated method stub
		return dataFromDevices;
	}

	public boolean setValues(ArrayList<Service> servicesToAccess, String message) {
		
		HashMap<String, HashMap<SensorData,Location>> dataFromDevices = new HashMap<String, HashMap<SensorData,Location>>();
		AccessGeneratorImpl ag = new AccessGeneratorImpl();
		DataFusionGeneratorImpl dg = new DataFusionGeneratorImpl();
		boolean responce;
		
		try {
		responce	 = ag
					.AccessDevice(servicesToAccess, message);

		} catch (NullPointerException e) {
			System.out
					.println("ExecutionGeneratorImpl.getValues: no devices to access");
			return false;
		}

		// TODO Auto-generated method stub
		return responce;
	}

}
