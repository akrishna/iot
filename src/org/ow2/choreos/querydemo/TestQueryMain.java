package org.ow2.choreos.querydemo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import java.util.ArrayList;
import java.util.Date;

import org.ow2.chereos.iots.common.BorderPoints;
import org.ow2.chereos.iots.common.Constants;
import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Point;
import org.ow2.chereos.iots.common.Service;
import org.ow2.choreos.lookup.impl.LookupManagerImpl;
import org.ow2.choreos.lookup.interfaces.LookupManager;
import org.ow2.choreos.query.constants.Selector;
import org.ow2.choreos.query.constants.Setter;
import org.ow2.choreos.query.constants.Where;
import org.ow2.choreos.query.impl.Query;
import org.ow2.choreos.queryiotsmanager.impl.ThingsQueryManagerImpl;
import org.ow2.choreos.sparqlfunctions.constants.Concept;

public class TestQueryMain {
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		new TestQueryMain();

	}

	TestQueryMain() throws IOException {

		boolean result = false;
		/*
		 * try { URL url = new URL(
		 * "http://things.vtrip.net:8080/RegistryManager-2.1/add_data");
		 * URLConnection conn = null;
		 * 
		 * try { conn = url.openConnection(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * conn.setDoOutput(true);
		 * 
		 * BufferedWriter out = null; try { out = new BufferedWriter(new
		 * OutputStreamWriter( conn.getOutputStream())); } catch (IOException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * 
		 * out.write(
		 * "address=http://things.vtrip.net:8080/proxy/123456/getnoiselevel&");
		 * out.write("device_id=1&"); out.write("name=getnoiselevel&");
		 * out.write("dataType=" + Constants.DOUBLENOISE + "&");
		 * out.write("dataDouble=34.7249&");
		 * out.write("sensor=noiselevel_sensor&"); out.write("unit=db&");
		 * out.write("global_region=Paris&"); out.write("concept=noiselevel&");
		 * out.write("x_coordinate=1&"); out.write("y_coordinate=1");
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { out.flush(); } catch (IOException e) {
		 * // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * out.close(); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } BufferedReader in = null; try { in = new
		 * BufferedReader(new InputStreamReader( conn.getInputStream())); try {
		 * String response = "";
		 * 
		 * while ((response = in.readLine()) != null) { if
		 * (response.equals("true")) result = true; else result = false; } }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { in.close(); } catch (IOException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } catch (MalformedURLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		/*
		 * result = false; try { URL url = new URL(
		 * "http://localhost:8080/RegistryManager/add_data"); URLConnection conn
		 * = null;
		 * 
		 * try { conn = url.openConnection(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * conn.setDoOutput(true);
		 * 
		 * BufferedWriter out = null; try { out = new BufferedWriter(new
		 * OutputStreamWriter( conn.getOutputStream())); } catch (IOException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * 
		 * out.write("address=URL3&"); out.write("device_id=3&");
		 * out.write("name=getnoiselevel&"); out.write("dataType=" +
		 * Constants.DOUBLENOISE + "&"); out.write("dataDouble=30.1455&");
		 * out.write("sensor=noiselevel_sensor&"); out.write("unit=db&");
		 * out.write("global_region=Paris&"); out.write("concept=noiselevel&");
		 * out.write("x_coordinate=10&"); out.write("y_coordinate=10");
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { out.flush(); } catch (IOException e) {
		 * // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * out.close(); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } BufferedReader in = null; try { in = new
		 * BufferedReader(new InputStreamReader( conn.getInputStream())); try {
		 * String response = "";
		 * 
		 * while ((response = in.readLine()) != null) { if
		 * (response.equals("true")) result = true; else result = false; } }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { in.close(); } catch (IOException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } catch (MalformedURLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * 
		 * result = false; try { URL url = new URL(
		 * "http://localhost:8080/RegistryManager/add_data"); URLConnection conn
		 * = null;
		 * 
		 * try { conn = url.openConnection(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * conn.setDoOutput(true);
		 * 
		 * BufferedWriter out = null; try { out = new BufferedWriter(new
		 * OutputStreamWriter( conn.getOutputStream())); } catch (IOException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * 
		 * out.write("address=URL11&"); out.write("device_id=11&");
		 * out.write("name=getnoiselevel&"); out.write("dataType=" +
		 * Constants.DOUBLENOISE + "&"); out.write("dataDouble=26.17&");
		 * out.write("sensor=noiselevel_sensor&"); out.write("unit=db&");
		 * out.write("global_region=Paris&"); out.write("concept=noiselevel&");
		 * out.write("x_coordinate=17&"); out.write("y_coordinate=17");
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { out.flush(); } catch (IOException e) {
		 * // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * out.close(); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } BufferedReader in = null; try { in = new
		 * BufferedReader(new InputStreamReader( conn.getInputStream())); try {
		 * String response = "";
		 * 
		 * while ((response = in.readLine()) != null) { if
		 * (response.equals("true")) result = true; else result = false; } }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { in.close(); } catch (IOException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } catch (MalformedURLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		// for (int i = 2; i < 12; i++) {
		// result = false;
		// try {
		// URL url = new URL(
		// "http://localhost:8080/RegistryManager/add_data");
		// URLConnection conn = null;
		//
		// try {
		// conn = url.openConnection();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// conn.setDoOutput(true);
		//
		// BufferedWriter out = null;
		// try {
		// out = new BufferedWriter(new OutputStreamWriter(
		// conn.getOutputStream()));
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// try {
		//
		// out.write("address=URL" + i + "&");
		// out.write("device_id=" + i + "&");
		// out.write("name=getLocation&");
		// out.write("dataType=" + Constants.DOUBLE2DLOCATION + "&");
		// out.write("dataDouble1=" + i + "&");
		// out.write("dataDouble2=" + (i + 1) + "&");
		// out.write("sensor=location_sensor&");
		// out.write("unit=latitude,longitude&");
		// out.write("global_region=Paris&");
		// out.write("concept=location&");
		// out.write("x_coordinate=2&");
		// out.write("y_coordinate=2");
		//
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// try {
		// out.flush();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// try {
		// out.close();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// BufferedReader in = null;
		// try {
		// in = new BufferedReader(new InputStreamReader(
		// conn.getInputStream()));
		// try {
		// String response = "";
		//
		// while ((response = in.readLine()) != null) {
		// if (response.equals("true"))
		// result = true;
		// else
		// result = false;
		// }
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// try {
		// in.close();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// } catch (MalformedURLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		/*
		 * result = false; try { URL url = new URL(
		 * "http://things.vtrip.net:8080/RegistryManager-2.1/add_data");
		 * URLConnection conn = null;
		 * 
		 * try { conn = url.openConnection(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * conn.setDoOutput(true);
		 * 
		 * BufferedWriter out = null; try { out = new BufferedWriter(new
		 * OutputStreamWriter( conn.getOutputStream())); } catch (IOException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * 
		 * out.write("address=URL17&"); out.write("device_id=17&");
		 * out.write("name=getPresence&"); out.write("dataType=" +
		 * Constants.BOOLEAN + "&"); out.write("dataBoolean=true&");
		 * out.write("sensor=infrared&"); out.write("unit=truefalse&");
		 * out.write("global_region=Airport&"); out.write("concept=presence&");
		 * out.write("x_coordinate=2&"); out.write("y_coordinate=2");
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { out.flush(); } catch (IOException e) {
		 * // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * out.close(); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } BufferedReader in = null; try { in = new
		 * BufferedReader(new InputStreamReader( conn.getInputStream())); try {
		 * String response = "";
		 * 
		 * while ((response = in.readLine()) != null) { if
		 * (response.equals("true")) result = true; else result = false; } }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { in.close(); } catch (IOException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } catch (MalformedURLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * 
		 * result = false; try { URL url = new URL(
		 * "http://things.vtrip.net:8080/RegistryManager-2.1/add_data");
		 * URLConnection conn = null;
		 * 
		 * try { conn = url.openConnection(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * conn.setDoOutput(true);
		 * 
		 * BufferedWriter out = null; try { out = new BufferedWriter(new
		 * OutputStreamWriter( conn.getOutputStream())); } catch (IOException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * 
		 * out.write("address=URL18&"); out.write("device_id=18&");
		 * out.write("name=getPressure&"); out.write("dataType=" +
		 * Constants.DOUBLEPRESSUREPA + "&"); out.write("dataDouble=10&");
		 * out.write("sensor=barometer&"); out.write("unit=pa&");
		 * out.write("global_region=Airport&"); out.write("concept=pressure&");
		 * out.write("x_coordinate=2&"); out.write("y_coordinate=2");
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { out.flush(); } catch (IOException e) {
		 * // TODO Auto-generated catch block e.printStackTrace(); } try {
		 * out.close(); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } BufferedReader in = null; try { in = new
		 * BufferedReader(new InputStreamReader( conn.getInputStream())); try {
		 * String response = "";
		 * 
		 * while ((response = in.readLine()) != null) { if
		 * (response.equals("true")) result = true; else result = false; } }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { in.close(); } catch (IOException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } catch (MalformedURLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		// ThingsQueryManagerImpl qm = new ThingsQueryManagerImpl();

		// ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
		// conceptsToMeasure.add(new Concept("pressure"));
		// Location locationOfInterest = new Location("Airport");
		// Query myQuery = new Query(new Selector(new Concept("pressure")),
		// new Setter(new Where("pressure.unit.equals=pa")),
		// new Location("Airport", 0, 0));
		// myQuery.setConstraint(W)
		// System.out.println("the final answer is"
		// + qm.getData(myQuery, Constants.DOUBLEPRESSUREPA, "average"));

		// Query main = new Query();
		// ThingsQueryManagerImpl qm1 = new ThingsQueryManagerImpl();

		/*
		 * conceptsToMeasure = new ArrayList<Concept>();
		 * conceptsToMeasure.add(new Concept("noiselevel")); locationOfInterest
		 * = new Location("Airport"); Query myQuery1 = new Query(new
		 * Selector(new Concept("noiselevel")), new Setter(new
		 * Where("noiselevel.unit.equals=db")), new Location("Airport", 0, 0));
		 * // myQuery.setConstraint(W) System.out.println("the final answer is"
		 * + qm1.getData(myQuery1, Constants.DOUBLENOISE, "average"));
		 */
		//
		// main = new Query();
		// ThingsQueryManagerImpl qm2 = new ThingsQueryManagerImpl();
		//
		// conceptsToMeasure = new ArrayList<Concept>();
		// conceptsToMeasure.add(new Concept("location"));
		// locationOfInterest = new Location("Airport");
		// Query myQuery2 = new Query(
		// new Selector(new Concept("location")),
		// new Setter(new Where("location.unit.equals=latitude,longitude")),
		// new Location("Airport"));
		// // myQuery.setConstraint(W)
		// System.out.println("the final answer is"
		// + qm2.getData(myQuery2, Constants.DOUBLE2DLOCATION,
		// "Counter;2,2;2,6;6,6;6,2"));

		/*
		 * main = new Query(); ThingsQueryManagerImpl qm3 = new
		 * ThingsQueryManagerImpl();
		 * 
		 * conceptsToMeasure = new ArrayList<Concept>();
		 * conceptsToMeasure.add(new Concept("presence")); locationOfInterest =
		 * new Location("Airport"); Query myQuery3 = new Query(new Selector(new
		 * Concept("presence")), new Setter(new
		 * Where("presence.unit.equals=truefalse")), new Location("Airport", 0,
		 * 0)); // myQuery.setConstraint(W)
		 * System.out.println("the final answer is" + qm3.getData(myQuery3,
		 * Constants.BOOLEAN));
		 * 
		 * /*Query myQueryAct = new Query(new Selector(new Concept("display")),
		 * new Setter(new Where("display.unit.equals=pixel")), new
		 * Location("Airport", 0, 0));
		 * 
		 * System.out.println("the final answer is" + qm.setData(myQueryAct,
		 * Constants.BOOLEANDISPLAY, "hello"));
		 */

		LookupManager le = new LookupManagerImpl();
		ArrayList<Concept> concepts = new ArrayList<Concept>();
		ArrayList<Double> distances = new ArrayList<Double>();
		distances.add(50.0);
		concepts.add(new Concept("temperature"));

	//	 ArrayList<Service> selectedServices = le.findDevicesInSubRegion(
	//	 concepts, new Location("Paris", new Point(0, 0), new Point(5, 5)));
		 ArrayList<Service> selectedServices =
		 le.findSubSetOfDevicesBasedOnCoverage(concepts, new
		 Location("Paris"), distances, 2.0, 2.0, "normal", 5.0, new Date().getTime());
//		ArrayList<Service> selectedServices = le
//				.findSubSetOfDevicesBasedOnCoverage(concepts, new Location(
//						"Paris", new BorderPoints(new Point(0, 0), new Point(
//								10, 0), new Point(0, 10), new Point(10, 10))),
//						"uniform", 80, new Date().getTime());
		System.out.println(selectedServices.size());
	}

}
