package org.ow2.choreos.querydemo;

import java.io.IOException;

import java.util.ArrayList;


import org.ow2.chereos.iots.common.BorderPoints;
import org.ow2.chereos.iots.common.Constants;
import org.ow2.chereos.iots.common.Location;
import org.ow2.chereos.iots.common.Point;
import org.ow2.choreos.lookup.impl.LookupManagerImpl;
import org.ow2.choreos.query.constants.Selector;
import org.ow2.choreos.query.constants.Setter;
import org.ow2.choreos.query.constants.Where;
import org.ow2.choreos.query.impl.Query;
import org.ow2.choreos.queryiotsmanager.impl.ThingsQueryManagerImpl;
import org.ow2.choreos.sparqlfunctions.constants.Concept;

public class SampleQuery {
	public static void main(String[] args) throws IOException {

		new SampleQuery();

	}

	SampleQuery() throws IOException {

		ArrayList<Concept> concepts = new ArrayList<Concept>();

		ThingsQueryManagerImpl qm1 = new ThingsQueryManagerImpl();

		concepts = new ArrayList<Concept>();
		concepts.add(new Concept("temperature"));
//
//		Query myQuery1 = new Query(new Selector(new Concept("noiselevel")),
//				new Setter(new Where("noiselevel.unit.equals=db")),
//				new Location("Airport", 0, 0));
//		
//		System.out.println("the final answer is"
//				+ qm1.getData(myQuery1, Constants.DOUBLENOISE, "average"));

		Location locOfInterest = new Location("Beijing",
				new BorderPoints(new Point(5725.46, 4436.88),
						new Point(5764.98, 4436.88), new Point(
								5725.46, 4481.36), new Point(
								5764.98, 4481.36)));
		// Location locOfInterest = new Location("Beijing",
		// new BorderPoints(new Point(5700, 4425),
		// new Point(6170, 4425), new Point(
		// 5700, 4481.36), new Point(
		// 6170, 4481.36)));
		LookupManagerImpl lm = new LookupManagerImpl();
		try{
		lm.findSubSetOfDevicesBasedOnCoverageSim(concepts,
				locOfInterest, "uniform", 60,
				1);}
		catch(NullPointerException e){
			e.printStackTrace();
			System.out.println("error executing lookup");
	
		}

	}

}
